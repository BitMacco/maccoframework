//
//  ViewController.swift
//  Example
//
//  Created by Emmanuel Okwara on 17/09/2020.
//  Copyright © 2020 Macco. All rights reserved.
//

import UIKit
import MaccoFramework

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("Valid email: ", MyValidation.isValidEmail(email: "testing@test.com"))
    }


}

