Pod::Spec.new do |spec|
  spec.name         = "MaccoFramework"
  spec.version      = "1.0.1"
  spec.summary      = "This is the best framework in the world"
  spec.description  = "This is definitely the best in the entire world"
  spec.homepage     = "https://BitMacco@bitbucket.org/BitMacco/maccoframework"
  spec.license      = "MIT"
  spec.author             = { "Emmanuel Okwara" => "emma4real37@gmail.com" }
  spec.platform     = :ios, "13.0"
  spec.source       = { :git => "https://BitMacco@bitbucket.org/BitMacco/maccoframework.git", :tag => spec.version.to_s }
  spec.source_files  = "MaccoFramework/**/*.{swift}"
  spec.swift_versions = "5.0"
end
