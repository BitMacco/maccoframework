//
//  MyValidation.swift
//  MaccoFramework
//
//  Created by Emmanuel Okwara on 17/09/2020.
//  Copyright © 2020 Macco. All rights reserved.
//

import Foundation

public struct MyValidation {
    private init() {}
    
    public static func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    static func sayHello() {
        print("How ya doing pretty lady?")
    }
}
